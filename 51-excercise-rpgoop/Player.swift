//
//  Player.swift
//  51-excercise-rpgoop
//
//  Created by Mac on 03/02/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation

class Player: Character {
    
    private var _name: String = "Noname"
    
    var name: String {
        get {
            return _name
        }
    }
    
    convenience init(name: String, hp: Int, attackPwr: Int) {
        self.init(hp: hp, attackPwr: attackPwr)
        
        _name = name
    }
    
}