//
//  Character.swift
//  51-excercise-rpgoop
//
//  Created by Mac on 03/02/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation

class Character {
    
    private var _hp: Int = 100
    private var _attackPwr: Int = 10
    
    var hp: Int {
        get {
            return _hp
        }
    }
    
    var attackPwr: Int {
        get {
            return _attackPwr
        }
    }
    
    init(hp: Int, attackPwr: Int) {
        self._hp = hp
        self._attackPwr = attackPwr
    }
    
    func isAlive() -> Bool {
        if self.hp <= 0 {
            return false
        }
        
        return true
    }
    
    func onAttack(attackPwr: Int) -> Bool {
        self._hp -= attackPwr
        
        return true
    }
    
}