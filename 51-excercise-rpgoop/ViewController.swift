//
//  ViewController.swift
//  51-excercise-rpgoop
//
//  Created by Mac on 03/02/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var playerLeftImg: UIImageView!
    
    @IBOutlet weak var playerRightImg: UIImageView!
    
    @IBOutlet weak var rightHpLbl: UILabel!
    
    @IBOutlet weak var leftHpLbl: UILabel!
    
    @IBOutlet weak var printLbl: UILabel!
    
    @IBOutlet weak var attackLeftBtn: UIButton!
    
    @IBOutlet weak var attackRightBtn: UIButton!
    
    var playerLeft: Player!
    var playerRight: Player!
    
    var attackSound: AVAudioPlayer!
    var deathSound: AVAudioPlayer!
    var backgroundMusic: AVAudioPlayer!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let randomAttackPwrPlayerLeft: Int = (Int)(arc4random_uniform(20) + 10)
        let randomAttackPwrPlayerRight: Int = (Int)(arc4random_uniform(20) + 10)

        playerLeft = Player(name: "Player1", hp: 100, attackPwr: randomAttackPwrPlayerLeft)
        
        playerRight = Player(name: "Player2", hp: 90, attackPwr: randomAttackPwrPlayerRight)
        
        leftHpLbl.text = "\(playerLeft.hp) HP"
        rightHpLbl.text = "\(playerRight.hp) HP"
        
        printLbl.text = "The battle starts between \(playerLeft.name) and \(playerRight.name)"
        
        activateRandomAttackBtn()
        
        let pathToAttackSound = NSBundle.mainBundle().pathForResource("attack", ofType: "wav")
        let soundAttackURL = NSURL(fileURLWithPath: pathToAttackSound!)
        
        do {
            try attackSound = AVAudioPlayer(contentsOfURL: soundAttackURL)
            attackSound.prepareToPlay()
        } catch let err as NSError {
            print("Error with attackSoundBtn \(err.debugDescription)")
        }
        
        let pathToDeathSound = NSBundle.mainBundle().pathForResource("death", ofType: "wav")
        let soundDeathURL = NSURL(fileURLWithPath: pathToDeathSound!)
        
        do {
            try deathSound = AVAudioPlayer(contentsOfURL: soundDeathURL)
            deathSound.prepareToPlay()
        } catch let err as NSError {
            print("Error with attackSoundBtn \(err.debugDescription)")
        }
        
        let pathToBgMusic = NSBundle.mainBundle().pathForResource("bground", ofType: "mp3")
        let bgMusicURL = NSURL(fileURLWithPath: pathToBgMusic!)
        
        do {
            try backgroundMusic = AVAudioPlayer(contentsOfURL: bgMusicURL)
            backgroundMusic.prepareToPlay()
        } catch let err as NSError {
            print("Error with attackSoundBtn \(err.debugDescription)")
        }
        
        backgroundMusic.play()
        
    }    
    
    func activateAttackBtnFor(timer: NSTimer) {
        let side = timer.userInfo as! String
                
        if side == "right" {
            
            attackRightBtn.enabled = true
            if let image = UIImage(named: "player1attackbtn") {
                attackRightBtn.setBackgroundImage(image, forState: .Normal)
            }
            
        } else if side == "left" {
            
            attackLeftBtn.enabled = true
            if let image = UIImage(named: "player1attackbtn") {
                attackLeftBtn.setBackgroundImage(image, forState: .Normal)
            }
            
        }
    }
    
    func disableAttackBtnFor(side: String) {
        
        // Disabling both users attack buttons and activating button to current attacker in 3 seconds
        if let imageLeftBtn = UIImage(named: "player2attackbtn") {
            attackLeftBtn.setBackgroundImage(imageLeftBtn, forState: .Normal)
        }
        
        if let imageRightBtn = UIImage(named: "player2attackbtn") {
            attackRightBtn.setBackgroundImage(imageRightBtn, forState: .Normal)
        }
        
        attackLeftBtn.enabled = false
        attackRightBtn.enabled = false
        
        if side == "left" {
            
//            attackLeftBtn.enabled = false
//            if let image = UIImage(named: "player2attackbtn") {
//                attackLeftBtn.setBackgroundImage(image, forState: .Normal)
//            }
            NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("activateAttackBtnFor:"), userInfo: "right", repeats: false)
        } else if side == "right" {
//            attackRightBtn.enabled = false
//            if let image = UIImage(named: "player2attackbtn") {
//                attackRightBtn.setBackgroundImage(image, forState: .Normal)
//            }
            NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("activateAttackBtnFor:"), userInfo: "left", repeats: false)

        } else if side == "both" {
            // Needs to keep buttons off if someone wins.
        }
    }
    
    func activateRandomAttackBtn() {
        let randomInt: Int = (Int)(arc4random_uniform(2) + 1)
        
        if randomInt == 1 {
            disableAttackBtnFor("left")
        } else {
            disableAttackBtnFor("right")
        }
    }
    
    func playersAttack(side: String, attackingPlayer: Player!, shieldingPlayer: Player!) {
        
        attackSound.play()
        
        if shieldingPlayer.onAttack(attackingPlayer.attackPwr) {
            printLbl.text = "\(attackingPlayer.name) attacked \(shieldingPlayer.name) for \(shieldingPlayer.attackPwr) HP"
            
            if !shieldingPlayer.isAlive() {
                
                deathSound.play()
                
                printLbl.text = "\(attackingPlayer.name) has WON!"
                disableAttackBtnFor("both")
                
                if side == "left" {
                    rightHpLbl.hidden = true
                    playerRightImg.hidden = true
                } else {
                    playerLeftImg.hidden = true
                    leftHpLbl.hidden = true
                }
                
            } else if side == "left" {
                rightHpLbl.text = "\(shieldingPlayer.hp) HP"
                disableAttackBtnFor(side)
            } else {
                leftHpLbl.text = "\(shieldingPlayer.hp) HP"
                disableAttackBtnFor(side)
            }
        }
        
        
    }
    
    @IBAction func onTappedLeftAttackBtn(sender: AnyObject) {
        playersAttack("left", attackingPlayer: playerLeft, shieldingPlayer: playerRight)
    }
    

    @IBAction func onTappedRightAttackBtn(sender: AnyObject) {
        playersAttack("right", attackingPlayer: playerRight, shieldingPlayer: playerLeft)
    }

}

